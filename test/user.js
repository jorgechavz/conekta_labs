const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const should = chai.should();
const User = require('../models').User;
const Wallet = require('../models').Wallet;
const cleardb = require('./util/cleandb');
chai.use(chaiHttp);

describe('User', () => {

  const newUser = {
    name: 'Jorge Chávez',
    phone: '6141179192',
    email: 'jorgechavzns@gmail.com'
  };

  before((done) => {
    User.sync({ force : true }) // drops table and re-creates it
      .then(() => {
        Wallet.sync({force: true})
          .then(() => {
            done();
          });
      });
  });

  beforeEach((done) => {
    cleardb.createUser(newUser, chai).then((res) => {
      done();
    });
  });

  it('should /GET all users', (done) => {

    chai.request(server)
      .get('/api/user')
      .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          res.body[0].should.have.property('name').eql(newUser.name);
          res.body[0].should.have.property('phone').eql(newUser.phone);
          res.body[0].should.have.property('wallet');
          done();
      });

  });

  it('should /POST a new user', (done) => {

    let otherUser = {
      name: 'Pedro Duarte',
      phone: '123456789',
      email: 'pedroduarte@gmail.com'
    };

    cleardb.createUser(otherUser, chai).then((res) => {
      res.should.have.status(200);
      res.body.should.have.property('name').eql(otherUser.name);
      res.body.should.have.property('phone').eql(otherUser.phone);
      res.body.should.have.property('email').eql(otherUser.email);
      done();
    });

  });

  it('should /GET one user by id', (done) => {

    chai.request(server)
      .get('/api/user/1')
      .end((err, res) => {
          res.should.have.status(200);
          done();
      });

  });

  it('should /PUT a user', (done) => {

    const newUser = {
      name: 'Jorge Chávez',
      phone: '6141179192',
      email: 'jorgechavzns@gmail.com'
    };

    const modifiedUser = {
      name: 'Jorge Chávez Palma',
      phone: '6141179192',
      email: 'jorgechavzns@gmail.com'
    };

    cleardb.createUser(newUser, chai).then((res) => {
      chai.request(server)
        .put(`/api/user/${res.body.id}`)
        .send(modifiedUser)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.have.property('name').eql(modifiedUser.name);
          res.body.should.have.property('phone').eql(modifiedUser.phone);
          res.body.should.have.property('email').eql(modifiedUser.email);
          done();
        });
    });

  });

  it('should /DELETE a user', (done) => {

    chai.request(server)
      .delete('/api/user/1')
      .end((err, res) => {

        res.should.have.status(204);

        chai.request(server)
          .get('/api/user/1')
          .end((err, res) => {
            should.not.exist(res.body);
            done();
          });

      });

  });

});
