const chai = require('chai');
const should = chai.should();
const RevenueUtil = require('../util/revenue');


describe('Revenue', () => {
  it('should calculate the revenue less than 1000', (done) => {
    var amount = 1000;
    var result = RevenueUtil.calculateRevenue(amount);
    var expectedResult = {
      amount: amount,
      percentage: 3.0,
      tasa: 8,
      finalAmount: 1038,
      conektaBalance: 38
    }
    result.should.be.eql(expectedResult);

    done();
  });

  it('should calculate the revenue between 1000 and 5000', (done) => {
    var amount = 4000;
    var result = RevenueUtil.calculateRevenue(amount);
    var expectedResult = {
      amount: amount,
      percentage: 2.5,
      tasa: 6,
      finalAmount: 4106,
      conektaBalance: 106
    }
    result.should.be.eql(expectedResult);
    done();
  });

  it('should calculate the revenue between 5000 and 10000', (done) => {
    var amount = 9000;
    var result = RevenueUtil.calculateRevenue(amount);
    var expectedResult = {
      amount: amount,
      percentage: 2.0,
      tasa: 4,
      finalAmount: 9184,
      conektaBalance: 184
    }
    result.should.be.eql(expectedResult);
    done();
  });

  it('should calculate the revenue greater than 10000', (done) => {
    var amount = 11000;
    var result = RevenueUtil.calculateRevenue(amount);
    var expectedResult = {
      amount: amount,
      percentage: 1.0,
      tasa: 3,
      finalAmount: 11113,
      conektaBalance: 113
    }
    result.should.be.eql(expectedResult);
    done();
  });
});
