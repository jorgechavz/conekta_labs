const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const should = chai.should();
const User = require('../models').User;
const Wallet = require('../models').Wallet;
const WalletHistory = require('../models').WalletHistory;
const ConektaBalance = require('../models').ConektaBalance;
const cleardb = require('./util/cleandb');
chai.use(chaiHttp);

describe('Conekta Balance', () => {

  before((done) => {
    User.sync({ force : true }) // drops table and re-creates it
      .then(() => {
        Wallet.sync({force: true})
          .then(() => {
            WalletHistory.sync({force: true})
              .then(() => {
                ConektaBalance.sync({force: true})
                  .then(() => {
                    ConektaBalance.create({
                      balance: 0
                    }).then(() => {
                      done();
                    });
                  });
              });
          });
      });
  });

  it('should add revenue to conekta when making a transaction', (done) => {

    var userOne = {
      name: 'Jorge',
      phone: '123456789',
      email: 'jorge@conekta.com',
      balance: 1000
    };

    var userTwo = {
      name: 'Leo',
      phone: '087654321',
      email: 'leo@conekta.com',
      balance: 100
    };

    Promise.all([
      cleardb.createUser(userOne, chai),
      cleardb.createUser(userTwo, chai)
    ]).then(responses => {

      userTwo.id = responses[1].body.id;
      var destWallet = responses[0].body.walletId;

      cleardb.createTransaction(userTwo.id, destWallet, 50,  chai)
        .then(firstTransaction => {

          chai.request(server)
            .get('/api/conekta')
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('array');
              res.body[0].should.have.property('balance').eql(9.5);
              done();
            });

        });

    });

  });

});
