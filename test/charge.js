const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const should = chai.should();
const User = require('../models').User;
const Wallet = require('../models').Wallet;
const cleardb = require('./util/cleandb');
chai.use(chaiHttp);

describe('Charge', () => {

  const newUser = {
    name: 'Jorge Chávez',
    phone: '6141179192',
    email: 'jorgechavzns@gmail.com'
  };

  before((done) => {
    User.sync({ force : true }) // drops table and re-creates it
      .then(() => {
        Wallet.sync({force: true})
          .then(() => {
            done();
          });
      });
  });

  beforeEach((done) => {
    cleardb.createUser(newUser, chai).then((res) => {
      newUser.id = res.body.id;
      newUser.walletId = res.body.walletId;
      done();
    });
  });

  it('should transfer money from card to wallet', (done) => {

    chai.request(server)
      .post('/api/charge')
      .send({
        card: '1234567890',
        cardHolder: 'Jorge Chavez',
        nss: '123',
        destinationWalletId: newUser.walletId,
        amount: 1200,
        userId: newUser.id,
        description: 'Payment from debit card'
      }).end((err, res) => {
        console.log(res.body);
        res.should.have.status(200);

        chai.request(server)
          .get(`/api/wallet/${newUser.walletId}`)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.balance.should.be.eql(1200);
            done();

          });

      });


  });

});
