const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const should = chai.should();
const User = require('../models').User;
const Wallet = require('../models').Wallet;
const WalletHistory = require('../models').WalletHistory;
const cleardb = require('./util/cleandb');
const RevenueUtil = require('../util/revenue');
chai.use(chaiHttp);

before((done) => {
  User.sync({ force : true }) // drops table and re-creates it
    .then(() => {
      Wallet.sync({force: true})
        .then(() => {
          WalletHistory.sync({force: true})
            .then(() => done());
        });
    });
});

describe('Wallet', () => {

  it('should transfer money from one wallet to another', (done) => {

    var userOne = {
      name: 'Jorge',
      phone: '123456789',
      email: 'jorge@conekta.com'
    };

    var userTwo = {
      name: 'Leo',
      phone: '087654321',
      email: 'leo@conekta.com',
      balance: 500
    };

    Promise.all([
      cleardb.createUser(userOne, chai),
      cleardb.createUser(userTwo, chai)
    ]).then(responses => {
      userTwo.id = responses[1].body.id;
      var destWallet = responses[0].body.walletId;

      chai.request(server)
        .post('/api/wallet')
        .send({
          amount: 300,
          userId: userTwo.id,
          destinationWallet: destWallet,
          description: 'Pago de mensualidad'
        }).end((err, res) => {          
          res.should.have.status(200);
          res.body.should.have.property('prevOriginWallet');
          res.body.prevOriginWallet.balance.should.be.eql(500);
          res.body.originWallet.balance.should.be.eql(183);
          res.body.prevDestinaitionWallet.balance.should.be.eql(0);
          res.body.destinationWallet.balance.should.be.eql(300);
          done();
        });

    });

  });

  it('should throw an error when amount is greater than the actual balance', (done) => {

    var userOne = {
      name: 'Jorge',
      phone: '123456789',
      email: 'jorge@conekta.com'
    };

    var userTwo = {
      name: 'Leo',
      phone: '087654321',
      email: 'leo@conekta.com',
      balance: 500
    };

    Promise.all([
      cleardb.createUser(userOne, chai),
      cleardb.createUser(userTwo, chai)
    ]).then(responses => {
      userTwo.id = responses[1].body.id;
      var destWallet = responses[0].body.walletId;

      chai.request(server)
        .post('/api/wallet')
        .send({
          amount: 600,
          userId: userTwo.id,
          destinationWallet: destWallet
        }).end((err, res) => {
          res.should.have.status(403);
          res.body.should.have.property('message').to.eql('Amount not valid');
          done();
        });

    });

  });

  it('should throw an error when user passed is not found', (done) => {

    var userOne = {
      name: 'Jorge',
      phone: '123456789',
      email: 'jorge@conekta.com'
    };

    var userTwo = {
      name: 'Leo',
      phone: '087654321',
      email: 'leo@conekta.com',
      balance: 500
    };

    Promise.all([
      cleardb.createUser(userOne, chai),
      cleardb.createUser(userTwo, chai)
    ]).then(responses => {
      var destWallet = responses[0].body.walletId;
      chai.request(server)
        .post('/api/wallet')
        .send({
          amount: 600,
          userId: 1000000,
          destinationWallet: destWallet
        }).end((err, res) => {
          res.should.have.status(403);
          res.body.should.have.property('message').to.eql('User not found');
          done();
        });

    });

  });

  it('should throw an error when destination wallet passed is not found', (done) => {

    var userOne = {
      name: 'Jorge',
      phone: '123456789',
      email: 'jorge@conekta.com'
    };

    var userTwo = {
      name: 'Leo',
      phone: '087654321',
      email: 'leo@conekta.com',
      balance: 500
    };

    Promise.all([
      cleardb.createUser(userOne, chai),
      cleardb.createUser(userTwo, chai)
    ]).then(responses => {
      userTwo.id = responses[1].body.id;

      chai.request(server)
        .post('/api/wallet')
        .send({
          amount: 300,
          userId: userTwo.id,
          destinationWallet: 10000,
          description: 'Monthly payment'
        }).end((err, res) => {
          res.should.have.status(403);
          res.body.should.have.property('message').to.eql('Destination wallet doesn\'t exist');
          done();
        });

    });

  });

});
