const server = require('../../app');
const User = require('../../models').User;

function cleanDatabase() {
  return new Promise((resolve, reject) => {
    User.sync({ force : true }) // drops table and re-creates it
      .then(() => {
        resolve(null);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

function createUser(newUser ,chai) {
  return new Promise((resolve, reject) => {
    chai.request(server)
      .post('/api/user')
      .send(newUser)
      .end((err, res) => {
        if(err) reject(err);
        resolve(res);
      });
  });
}

function createTransaction(userId, destinationWallet, amount,  chai) {

  return new Promise(function(resolve, reject) {
    chai.request(server)
      .post('/api/wallet')
      .send({
        amount: amount,
        userId: userId,
        destinationWallet: destinationWallet,
        description: 'Payment ' + makeid()
      }).end((err, res) => {
        resolve(res);
      });
  });

}

function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

module.exports = {
  createUser,
  cleanDatabase,
  createTransaction
};
