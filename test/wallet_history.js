const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const should = chai.should();
const User = require('../models').User;
const Wallet = require('../models').Wallet;
const WalletHistory = require('../models').WalletHistory;
const cleardb = require('./util/cleandb');
chai.use(chaiHttp);

describe('WalletHistory', () => {

  var newUser = {
    name: 'Jorge Chávez',
    phone: '6141179192',
    email: 'jorgechavzns@gmail.com'
  };

  before((done) => {
    User.sync({ force : true }) // drops table and re-creates it
      .then(() => {
        Wallet.sync({force: true})
          .then(() => {
            WalletHistory.sync({force: true})
              .then(() => {
                done();
              });
          });
      });
  });

  beforeEach((done) => {
    cleardb.createUser(newUser, chai).then((res) => {
      newUser.id = res.body.id;
      done();
    });
  });

  it('should get all history from a user', (done) => {

    var userOne = {
      name: 'Jorge',
      phone: '123456789',
      email: 'jorge@conekta.com',
      balance: 1000
    };

    var userTwo = {
      name: 'Leo',
      phone: '087654321',
      email: 'leo@conekta.com',
      balance: 100
    };

    Promise.all([
      cleardb.createUser(userOne, chai),
      cleardb.createUser(userTwo, chai)
    ]).then(responses => {

      userTwo.id = responses[1].body.id;
      var destWallet = responses[0].body.walletId;

      cleardb.createTransaction(userTwo.id, destWallet, 50,  chai)
        .then(firstTransaction => {

          chai.request(server)
            .get(`/api/wallet-history/${userTwo.id}`)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('array').to.length(1);
              // 50 + 1.5 + 8 = 59.5
              res.body[0].amount.should.be.eql(59.5);
              res.body[0].user.name.should.be.eql(userTwo.name);
              res.body[0].user.email.should.be.eql(userTwo.email);
              res.body[0].type.should.be.eql('outcome');
              res.body[0].user.walletId.should.be.eql(responses[1].body.walletId);
              done();
            });

        });

    });

  });


});
