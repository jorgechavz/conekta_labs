var userController = require('../../controllers/user');

module.exports = (router) => {

  /**
     * @api {get} /api/user Get all
     * @apiName list
     * @apiGroup User
     * @apiVersion 1.0.0
     * @apiErrorExample Response (example):
     [
          {
              "id": 1,
              "name": "Jorge",
              "phone": "123456789",
              "email": "jchavez@gmail.com",
              "createdAt": "2017-12-27T01:59:49.527Z",
              "updatedAt": "2017-12-27T01:59:49.527Z",
              "walletId": 1,
              "wallet": {
                  "id": 1,
                  "balance": 0
              }
          },
          {
              "id": 2,
              "name": "Leo",
              "phone": "0987654321",
              "email": "leo@gmail.com",
              "createdAt": "2017-12-27T05:06:35.664Z",
              "updatedAt": "2017-12-27T05:06:35.664Z",
              "walletId": 2,
              "wallet": {
                  "id": 2,
                  "balance": 0
              }
          }
      ]
    */
  router.get('/user', userController.list);


  /**
     * @api {get} /api/user/:userId Get one
     * @apiName retrieve
     * @apiGroup User
     * @apiVersion 1.0.0
     * @apiErrorExample Response (example):
     {
          "id": 1,
          "name": "Jorge",
          "phone": "123456789",
          "email": "jchavez@gmail.com",
          "createdAt": "2017-12-27T01:59:49.527Z",
          "updatedAt": "2017-12-27T01:59:49.527Z",
          "walletId": 1,
          "wallet": {
              "id": 1,
              "balance": 0
          }
      }
    */
  router.get('/user/:userId', userController.retrive);

  /**
   * @api {post} /api/user Create
   * @apiName create
   * @apiGroup User
   * @apiVersion 1.0.0
   * @apiParam {String} name Name of the user
   * @apiParam {String} phone Phone number
   * @apiParam {String} email Email

   * @apiErrorExample Response (example)
   *
   {
        "id": 3,
        "name": "Leo",
        "phone": "0987654321",
        "email": "leo@gmail.com",
        "walletId": 3,
        "updatedAt": "2017-12-27T05:10:34.892Z",
        "createdAt": "2017-12-27T05:10:34.892Z"
    }
  */
  router.post('/user', userController.create);

  /**
   * @api {put} /api/user/:userId Edit
   * @apiName edit
   * @apiGroup User
   * @apiVersion 1.0.0
   * @apiParam {String} name Name of the user
   * @apiParam {String} phone Phone number
   * @apiParam {String} email Email

   * @apiErrorExample Response (example)
   *
   {
        "id": 3,
        "name": "Ruby",
        "phone": "0987654321",
        "email": "ruby@gmail.com",
        "createdAt": "2017-12-27T05:10:34.892Z",
        "updatedAt": "2017-12-27T05:15:48.673Z",
        "walletId": 3
    }
  */
  router.put('/user/:userId', userController.edit);

  /**
   * @api {delete} /api/user/:userId Delete
   * @apiName destroy
   * @apiGroup User
   * @apiVersion 1.0.0

   * @apiErrorExample Response (example)
   *
   []
  */
  router.delete('/user/:userId', userController.destroy);

}
