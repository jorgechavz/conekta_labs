const express = require('express');
const router = express.Router();
const userRoute = require('./user');
const walletRoute = require('./wallet');
const walletHistoryRoute = require('./wallet_history');
const conektaBalanceRoute = require('./conekta_balance');
const chargeRoute = require('./charge');

userRoute(router);
walletRoute(router);
walletHistoryRoute(router);
conektaBalanceRoute(router);
chargeRoute(router);

module.exports = router;
