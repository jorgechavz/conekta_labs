const walletHistoryController = require('../../controllers/wallet_history');

module.exports = (router) => {



   /**
    * @api {get} /api/wallet/:walletId Get one
    * @apiName retrieve
    * @apiGroup Wallet
    * @apiVersion 1.0.0

    * @apiErrorExample Response (example)
    *
    [
      {
        id: 1,
        description: 'Payment description',
        type: 'outcome',
        amount: 59.5,
        user:
         { id: 3,
           name: 'Leo',
           phone: '087654321',
           email: 'leo@conekta.com',
           walletId: 3
         }
      }
    ]
   */
  router.get('/wallet-history/:userId', walletHistoryController.list);

}
