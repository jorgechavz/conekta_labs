const walletController = require('../../controllers/wallet');

module.exports = (router) => {

  /**
   * @api {post} /api/wallet Create
   * @apiName create
   * @apiGroup Wallet
   * @apiVersion 1.0.0
   * @apiParam {double} amount Amount of money intended to transfer
   * @apiParam {integer} userId User ID who is sending the money
   * @apiParam {integer} destinationWallet Destination wallet ID
   * @apiParam {integer} description Description of the transaction

   * @apiErrorExample Response (example)
   *
   {
    prevOriginWallet: {
      id: 9,
      balance: 500
    },
    originWallet: {
      id: 9,
      balance: 183,
      updatedAt: '2017-12-27T06:45:04.910Z'
    },
    prevDestinaitionWallet: {
      id: 8,
      balance: 0
    },
    destinationWallet: {
      id: 8,
      balance: 300,
      updatedAt: '2017-12-27T06:45:04.913Z'
    },
    originRecord:
     {
        id: 4,
        userId: 8,
        description: 'Pago de mensualidad',
        type: 'income',
        amount: 300,
        updatedAt: '2017-12-27T06:45:04.919Z',
        createdAt: '2017-12-27T06:45:04.919Z'
      }
     }
  */
  router.post('/wallet', walletController.add);

  /**
   * @api {get} /api/wallet/:walletId Get one
   * @apiName retrieve
   * @apiGroup Wallet
   * @apiVersion 1.0.0

   * @apiErrorExample Response (example)
   *
   {
        "id": 1,
        "balance": 0,
        "createdAt": "2017-12-27T06:42:07.675Z",
        "updatedAt": "2017-12-27T06:42:07.675Z"
    }
  */
  router.get('/wallet/:walletId', walletController.retrieve);

}
