var chargeController = require('../../controllers/charge');

module.exports = (router) => {

  /**
   * @api {post} /api/charge Create
   * @apiName charge
   * @apiGroup Charge
   * @apiVersion 1.0.0
   * @apiParam {String} card Credit/Debit card number
   * @apiParam {String} cardHolder Card Holder name
   * @apiParam {String} nss Email
   * @apiParam {String} destinationWalletId Destination wallet
   * @apiParam {String} amount Amount to deposit
   * @apiParam {String} description Description of the transaction (optional)
   * @apiParam {String} userId User ID who sent the money


   * @apiErrorExample Response (example)
   *
   {
    id: 1,
    description: 'Payment from debit card',
    amount: 1200,
    userId: 1,
    type: 'charge',
    updatedAt: '2017-12-27T06:55:55.218Z',
    createdAt: '2017-12-27T06:55:55.218Z'
  }
  */
  router.post('/charge', chargeController.charge);

}
