var conektaBalance = require('../../controllers/conekta_balance');

module.exports = (router) => {

  /**
   * @api {get} /api/conekta Get all
   * @apiName list
   * @apiGroup Conekta
   * @apiVersion 1.0.0
   * @apiErrorExample Response (example):
   [
       {
           "id": 1,
           "balance": 36,
           "createdAt": "2017-12-27T06:49:33.187Z",
           "updatedAt": "2017-12-27T06:49:33.556Z"
       }
   ]
  */
  router.get('/conekta', conektaBalance.list);

}
