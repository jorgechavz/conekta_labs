function calculateRevenue(amount) {

  var percentage;
  var tasa;
  var finalAmount;

  if (amount <= 1000) {

    percentage = 3.0;
    tasa = 8;

  } else if (amount <= 5000) {

    percentage = 2.5;
    tasa = 6;

  } else if (amount <= 10000) {

    percentage = 2.0;
    tasa = 4;

  } else {

    percentage = 1.0;
    tasa = 3;

  }

  var per = (amount * (percentage / 100));
  finalAmount = amount + per + tasa;
  var conektaBalance = per + tasa;

  return {
    amount,
    percentage,
    tasa,
    finalAmount,
    conektaBalance
  }
}

module.exports = {
  calculateRevenue
}
