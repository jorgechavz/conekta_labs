// THIS IS AN FAKE GATEWAY
function transfer(transferenceObj) {
  return new Promise((resolve, reject) => {
    var receipt = {
      name: transferenceObj.cardHolder,
      amount: transferenceObj.amount,
    };
    resolve(receipt);
  });
};

module.exports = {
  transfer
};
