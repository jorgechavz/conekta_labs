const Validator = require('./validator');

function transfer(transferenceObj) {

  return new Promise((resolve, reject) => {
    Validator.getBankConnector(transferenceObj.card)
      .then(bank => {
        bank.transfer(transferenceObj)
          .then(receipt => {
              resolve(receipt);
          });

      });
  });

}


module.exports = {
  transfer
}
