const BanamexConnector = require('./banamex');
const SantanderConnector = require('./santander');
const BancomerConnector = require('./bancomer');

function getBankConnector(card) {
  // Some logic for chosing the Bank using Factory Pattern
  return new Promise((resolve, reject) => {
    resolve(BanamexConnector);
  });

}

module.exports = {
  getBankConnector
}
