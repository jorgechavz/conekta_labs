const ConektaBalance = require('../models').ConektaBalance;

function list(req, res) {
  ConektaBalance.findAll()
    .then(conektaBalance => {
      res.status(200).json(conektaBalance);
    });
};

module.exports = {
  list
}
