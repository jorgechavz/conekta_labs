const Wallet = require('../models').Wallet;
const WalletHistory = require('../models').WalletHistory;
const Connectors = require('../connectors'); // Fake gateway

function charge(req, res) {

  var card = req.body.card;
  var cardHolder = req.body.cardHolder;
  var nss = req.body.nss;
  var destinationWalletId = req.body.destinationWalletId;
  var amount = req.body.amount;
  var description = req.body.description;
  var userId = req.body.userId;

  var transferenceObj = {
    card,
    cardHolder,
    nss,
    amount
  };

  var initConnector = new Promise((resolve, reject) => {
    Connectors.transfer(transferenceObj)
      .then(receipt => {

        resolve(receipt);

      }).catch(err => {
        reject(err);
      });
  });

  initConnector.then(receipt => {

    // Change balance in wallet

    return new Promise((resolve, reject) => {

      Wallet.findById(destinationWalletId)
        .then(wallet => {
          if (!wallet) {
            throw Error('Wallet not found');
          };

          wallet.update({
            balance: wallet.balance + amount
          }).then(newWallet => {
              resolve(newWallet);
          });

        }).catch(err => reject(err.message));

    });

  }).then((newWallet) => {

    // Change history of user
    return new Promise((resolve, reject) => {

      WalletHistory.create({
        description,
        amount,
        userId,
        type: 'charge'
      }).then((walletHistoryRecord) => {
        res.status(200).json(walletHistoryRecord);
      });

    });

  }).catch(err => {
    res.status(203).json({message: err.message});
  })

}

module.exports = {
  charge
}
