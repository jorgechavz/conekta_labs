const User = require('../models').User;
const Wallet = require('../models').Wallet;

function list(req, res) {
  User.findAll({
    include: [{
      model: Wallet,
      as: 'wallet',
      attributes: { exclude: ['createdAt', 'updatedAt'] }
    }]
  }).then(users => {
      res.status(200).json(users);
    });
};

function create(req, res) {
  // Create a new Wallet
  Wallet.create({
    balance: req.body.balance || 0
  }).then(wallet => {
      //Create a new User with the new wallet
      User.create({
        name: req.body.name,
        phone: req.body.phone,
        email: req.body.email,
        walletId: wallet.id
      }).then(user => {
        res.status(200).json(user);
      });
  });
}

function retrive(req, res) {
  var userId = req.params.userId;
  User.findById(userId, {
    include: [{
      model: Wallet,
      as: 'wallet',
      attributes: { exclude: ['createdAt', 'updatedAt'] }
    }]
  }).then(user => {
      res.status(200).json(user);
    });
}

function destroy(req, res) {
  User.findById(req.params.userId)
    .then(user => {
      user.destroy()
        .then(user => {
        res.status(204).json(user);
      });
    });
}

function edit(req, res) {
  User.findById(req.params.userId)
    .then(user => {
      user.update({
        name: req.body.name || user.name,
        phone: req.body.phone || user.phone,
        email: req.body.email || user.email
      }).then(user => {
        res.status(200).json(user);
      });
    });
}

module.exports = {
  list,
  create,
  retrive,
  destroy,
  edit
};
