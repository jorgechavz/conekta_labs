const User = require('../models').User;
const WalletHistory = require('../models').WalletHistory;

function list(req, res) {
  WalletHistory.findAll({
    where: {
      userId: req.params.userId
    },
    include: [{
      model: User,
      as: 'user',
      attributes: { exclude: ['createdAt', 'updatedAt'] }
    }],
    attributes: { exclude: ['userId', 'createdAt', 'updatedAt'] }
  }).then(history => {
    res.status(200).json(history);
  });
}

module.exports = {
  list
};
