const User = require('../models').User;
const Wallet = require('../models').Wallet;
const WalletHistory = require('../models').WalletHistory;
const ConektaBalance = require('../models').ConektaBalance;
const RevenueUtil = require('../util/revenue');

function add(req, res) {

  var amount = req.body.amount;
  var userId = req.body.userId;
  var destinationWallet = req.body.destinationWallet;
  var description = req.body.description;
  var prevOriginWallet;
  var originWallet;
  var destUserId;
  var revenue = RevenueUtil.calculateRevenue(amount);

  var userFunc = new Promise((resolve, reject) => {
    User.findById(userId, {
      include: [{
        model: Wallet,
        as: 'wallet',
        attributes: { exclude: ['createdAt', 'updatedAt'] }
      }]
    }).then(user => {
        resolve(user);
      }).catch(err => {
        reject(err);
      });
  });

  userFunc.then(user => {
    //Check if user exist
    if (!user) {
        throw Error('User not found');
    }
    return user;
  }).then((user) => {
    //Check amount
    if (revenue.finalAmount > user.wallet.balance) {
      throw Error('Amount not valid');
    }
    return user;
  }).then(user => {
    //Find origin wallet
    return new Promise((resolve, reject) => {
      Wallet.findById(user.walletId, {
        attributes: { exclude: ['createdAt', 'updatedAt'] }
      }).then(clientWallet => {
          resolve(clientWallet);
      }).catch(err => {
        reject(err);
      });
    });

  }).then(clientWallet => {
    // Change origin wallet
    prevOriginWallet = Object.assign({}, clientWallet.dataValues);
    return new Promise((resolve, reject) => {
      clientWallet.update({
        balance: (clientWallet.balance - revenue.finalAmount)
      }).then(originWallet => {
        resolve(originWallet);
      }).catch(err => reject(err));
    });

  }).then(resOriginWallet => {
    // Find destination wallet
    originWallet = resOriginWallet.dataValues;
    return new Promise((resolve, reject) => {
      Wallet.findById(destinationWallet, {
        attributes: { exclude: ['createdAt', 'updatedAt'] }
      }).then(destWallet => {
        if (!destWallet) {
          throw Error('Destination wallet doesn\'t exist');
        }
        destUserId = destWallet.userId;
        resolve(destWallet);
      }).catch(err => {
        reject(err);
      });
    });

  }).then(destWallet => {
    //Update destination wallet with new amount

    let prevDestinaitionWallet = Object.assign({}, destWallet.dataValues);

    return new Promise((resolve, reject) => {

      destWallet.update({
        balance: (destWallet.balance + amount)
      }).then(resDestinationWallet => {

        destinationWallet = resDestinationWallet.dataValues;

        var result = {
          prevOriginWallet,
          originWallet,
          prevDestinaitionWallet,
          destinationWallet
        };

        resolve(result);

      }).catch(err => reject(err));

    });

  }).then(result => {
    //Add record in WalletHistory
    return new Promise((resolve, reject) => {
      WalletHistory.create({
        userId,
        description,
        type: 'outcome',
        amount: revenue.finalAmount
      }).then(record => {
        result.originRecord = record.dataValues;
        resolve(result);
      }).catch(err => reject(err));

    });

  }).then(result => {
    // Create record for dest user
    return new Promise((resolve, reject) => {
      User.findOne({
        where: {
          walletId: destinationWallet.id
        }
      }).then(user => {
        WalletHistory.create({
          userId: user.id,
          description,
          type: 'income',
          amount
        }).then(record => {
          result.originRecord = record.dataValues;
          resolve(result);
        }).catch(err => reject(err));

      }).catch(err => reject(err));

    });

  }).then(result => {
    // Add revenue for Conekta
    return new Promise((resolve, reject) => {
      ConektaBalance.findOne()
        .then(conektaBalance => {
          var newConektaBalance = conektaBalance.balance + revenue.conektaBalance;
          conektaBalance.update({
            balance: newConektaBalance
          }).then(newBalance => {
            resolve(result);
          }).catch(err => reject(err));
        }).catch(err => reject(err));
    });

  }).then(result => {

    res.status(200).json(result);

  }).catch(error => {
    res.status(403).json({message: error.message});
  });

}

function retrieve(req, res) {

  Wallet.findById(req.params.walletId)
    .then(wallet => {
      res.status(200).json(wallet);
    }).catch((err => {
      res.status(403).json({message: err.message});
    }));

}

module.exports = {
  add,
  retrieve
}
