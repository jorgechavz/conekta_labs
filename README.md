# Conekta Labs

## Environment setup

* Create a database called `conekta_labs`

* Install sequelize-cli with `npm install sequelize-cli -g`

* Clone the repository and run `npm install` in order to install all dependencies

* Run `sequelize db:migrate` in order to create the table schema

* Run the app with `node app.js`

## Verify env

* After running the app, go to `http://localhost:5000/`, you should be able to see the API documentation for each endpoint



## Extra features

* I'm using eslint in order to check syntax
* Using APIDOC in order to show better documentation
