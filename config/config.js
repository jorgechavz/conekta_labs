module.exports = {
  development: {
    username: 'postgres',
    password: 'postgres',
    database: 'conekta_labs',
    host: '127.0.0.1',
    dialect: 'postgres',
    logging: false
  },
  test: {
    username: 'root',
    password: null,
    database: 'database_test',
    host: '127.0.0.1',
    dialect: 'mysql'
  },
  production: {
    username: process.env.CONEKTA_DB_USERNAME,
    password: process.env.CONEKTA_DB_PASSWORD,
    database: process.env.CONEKTA_DB_DATABASE,
    host: process.env.CONEKTA_DB_HOST,
    dialect: process.env.CONEKTA_DB_DIALECT
  }
}
