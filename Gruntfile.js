module.exports = function (grunt) {
  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    eslint: {
        files: [
          'Gruntfile.js',
          'config/*.js',
          'models/*.js',
          'controllers/*.js',
          'routes/*.js',
          'test/*.js',
          'connectors/*.js',
          'util/*.js'
        ]
    },
    apidoc: {
      conektaApp: {
        src: 'routes/',
        dest: 'docs/'
      }
    },
    watch: {
      scripts: {
        files: ['routes/*.js'],
        tasks: ['docs'],
        options: {
          spawn: true
        },
      },
    },
  });

  // Default task(s).
  grunt.registerTask('default', ['eslint']);
  grunt.registerTask('docs', ['apidoc']);

};
