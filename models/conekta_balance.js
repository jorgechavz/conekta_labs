'use strict';
module.exports = (sequelize, DataTypes) => {
  var ConektaBalance = sequelize.define('ConektaBalance', {
    balance: DataTypes.DOUBLE
  });
  return ConektaBalance;
};
