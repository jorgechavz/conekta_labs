'use strict';
module.exports = (sequelize, DataTypes) => {
  var WalletHistory = sequelize.define('WalletHistory', {
    userId: DataTypes.INTEGER,
    description: DataTypes.TEXT,
    type: DataTypes.STRING,
    amount: DataTypes.DOUBLE
  });

  WalletHistory.associate = (models) => {
    WalletHistory.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'user'
    });
  };


  return WalletHistory;
};
