'use strict';
module.exports = (sequelize, DataTypes) => {
  var Wallet = sequelize.define('Wallet', {
    balance: DataTypes.DOUBLE
  });

  Wallet.associate = (models) => {
    Wallet.hasOne(models.User, {
      foreignKey: 'walletId',
      onDelete: 'CASCADE'
    });
  };

  return Wallet;
};
