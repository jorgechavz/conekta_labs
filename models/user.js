'use strict';
module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    name: DataTypes.STRING,
    phone: DataTypes.STRING,
    email: DataTypes.STRING
  });

  User.associate = (models) => {
    User.belongsTo(models.Wallet, {
      foreignKey: 'walletId',
      as: 'wallet'
    });
    User.hasMany(models.WalletHistory, {
      foreignKey: 'userId',
      onDelete: 'CASCADE'
    });
  };

  return User;
};
